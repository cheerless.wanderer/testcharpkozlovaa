﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;

namespace KozlovTestCsharpV2
{
    //Класс для работы с данными о сотрудниках
    public class EmployeeService
    {
        private DatabaseHelper databaseHelper;

        public EmployeeService(DatabaseHelper dbHelper)
        {
            databaseHelper = dbHelper;
        }

        //Метод для заполнения данных о сотрудниках
        public List<Employee> GetEmployees()
        {
            List<Employee> employees = new List<Employee>();
            string query = "GetPersonsData";
            SqlConnection connection = databaseHelper.GetConnection();
            SqlCommand command = new SqlCommand(query, connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            try
            {
                databaseHelper.OpenConnection();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Employee employee = new Employee
                    {
                        Id = reader.GetInt32(0),
                        FullName = reader.GetString(1),
                        DateEmploy = reader.IsDBNull(2) ? (DateTime?)null : reader.GetDateTime(2),
                        DateUnemploy = reader.IsDBNull(3) ? (DateTime?)null : reader.GetDateTime(3),
                        StatusName = reader.GetString(4),
                        DepartmentName = reader.GetString(5),
                        PostName = reader.GetString(6)
                    };

                    employees.Add(employee);
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception($"Ошибка загрузки данных сотрудников: {ex.Message}");
            }
            finally
            {
                databaseHelper.CloseConnection();
            }

            return employees;
        }

        //Метод для поиска сотрудников по параметрам
        public List<Employee> SearchEmployees(string filterParam, string filterValue)
        {
            List<Employee> employees = new List<Employee>();
            string query = "SearchEmployees";
            string id = GetIdByName(filterParam, filterValue);

            SqlConnection connection = databaseHelper.GetConnection();
            SqlCommand command = new SqlCommand(query, connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            command.Parameters.AddWithValue("@FilterParam", filterParam);
            command.Parameters.AddWithValue("@FilterValue", id);

            try
            {
                databaseHelper.OpenConnection();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Employee employee = new Employee
                    {
                        Id = reader.GetInt32(0),
                        FullName = reader.GetString(1),
                        DateEmploy = reader.IsDBNull(2) ? (DateTime?)null : reader.GetDateTime(2),
                        DateUnemploy = reader.IsDBNull(3) ? (DateTime?)null : reader.GetDateTime(3),
                        StatusName = reader.GetString(4),
                        DepartmentName = reader.GetString(5),
                        PostName = reader.GetString(6)
                    };

                    employees.Add(employee);
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception($"Ошибка поиска сотрудников: {ex.Message}");
            }
            finally
            {
                databaseHelper.CloseConnection();
            }

            return employees;
        }

        //Метод для получения Id по названию
        private string GetIdByName(string filterParam, string name)
        {
            string query = "";
            switch (filterParam)
            {
                case "По статусу":
                    query = "GetStatusIdByName";
                    break;
                case "По отделу":
                    query = "GetDepartmentIdByName";
                    break;
                case "По должности":
                    query = "GetPostIdByName";
                    break;
            };

            if (string.IsNullOrEmpty(query))
                return name;

            SqlConnection connection = databaseHelper.GetConnection();
            SqlCommand command = new SqlCommand(query, connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.AddWithValue("@Name", name);

            try
            {
                databaseHelper.OpenConnection();
                object result = command.ExecuteScalar();
                return result != null ? result.ToString() : "";
            }
            catch (Exception ex)
            {
                throw new Exception($"Ошибка получения ID: {ex.Message}");
            }
            finally
            {
                databaseHelper.CloseConnection();
            }
        }
    }

}
