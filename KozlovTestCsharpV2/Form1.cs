﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Configuration;

namespace KozlovTestCsharpV2
{
    public partial class Form1 : Form
    {
        private readonly DatabaseHelper databaseHelper;
        private readonly StatusService statusService;
        private readonly EmployeeService employeeService;

        public Form1()
        {
            InitializeComponent();
            //Данные находятся в файле App.config
            string connectionString = ConfigurationManager.ConnectionStrings["EmployeeDB"].ConnectionString;
            databaseHelper = new DatabaseHelper(connectionString);
            statusService = new StatusService(databaseHelper);
            employeeService = new EmployeeService(databaseHelper);

            CreateColumns();
            RefreshDataGrid(maintable);
            LoadStatusesComboBox();
        }

        //Метод загрузки статусов из БД для статистики
        private void LoadStatusesComboBox()
        {
            statusComboBox.Items.Clear();
            try
            {
                List<string> statuses = statusService.GetStatuses();
                statusComboBox.Items.AddRange(statuses.ToArray());
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка загрузки статусов: {ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Метод для загрузки шапки таблицы
        private void CreateColumns()
        {
            maintable.Columns.Clear();
            maintable.Columns.Add("id", "ID");
            maintable.Columns.Add("full_name", "Фамилия И.О.");
            maintable.Columns.Add("date_employ", "Дата приёма");
            maintable.Columns.Add("date_unemploy", "Дата увольнения");
            maintable.Columns.Add("status_name", "Статус");
            maintable.Columns.Add("department_name", "Отдел");
            maintable.Columns.Add("post_name", "Должность");
        }

        //Метод для ввода 1го ряда в таблицу
        private void ReadSingleRow(DataGridView dgw, Employee employee)
        {
            dgw.Rows.Add(employee.Id, employee.FullName, employee.DateEmploy, employee.DateUnemploy,
                         employee.StatusName, employee.DepartmentName, employee.PostName);
        }

        //Метод для заполнения таблицы значениями из БД
        private void RefreshDataGrid(DataGridView dgw)
        {
            dgw.Rows.Clear();
            try
            {
                List<Employee> employees = employeeService.GetEmployees();
                foreach (Employee employee in employees)
                {
                    ReadSingleRow(dgw, employee);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка обновления таблицы: {ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Обработчик событий кнопуи Отфильтровать
        private void filterBtn_Click(object sender, EventArgs e)
        {
            if (checkComboBox.Visible && checkComboBox.SelectedItem != null)
            {
                var selectedItem = checkComboBox.SelectedItem.ToString();
                if (!string.IsNullOrEmpty(selectedItem))
                {
                    SearchEmployees(maintable, filterComboBox.Text, selectedItem);
                    return;
                }
            }

            if (secondNameTextBox.Visible && !string.IsNullOrEmpty(secondNameTextBox.Text))
            {
                SearchEmployees(maintable, filterComboBox.Text, secondNameTextBox.Text);
                return;
            }

            MessageBox.Show("Ведены пустые значения", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            RefreshDataGrid(maintable);
        }

        //Метод поиска сотрудников по параметрам в БД
        private void SearchEmployees(DataGridView dgw, string filterParam, string filterValue)
        {
            dgw.Rows.Clear();
            try
            {
                List<Employee> employees = employeeService.SearchEmployees(filterParam, filterValue);
                foreach (Employee employee in employees)
                {
                    ReadSingleRow(dgw, employee);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка поиска сотрудников: {ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        //Обработчик событий выбора критерия поиска
        private void filterComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedFilter = filterComboBox.SelectedItem.ToString();
            switch (selectedFilter)
            {
                case "По статусу":
                    LoadCheckComboBox("GetEmployeeStatuses", "id", "name");
                    break;

                case "По отделу":
                    LoadCheckComboBox("GetDepartments", "id", "name");
                    break;

                case "По должности":
                    LoadCheckComboBox("GetPosts", "id", "name");
                    break;

                case "По части фамилии":
                    checkComboBox.Visible = false;
                    checkComboBox.Text = "";
                    secondNameTextBox.Visible = true;
                    break;
            }
        }

        //Метод загрузки выпадающего списка критериев поиска
        private void LoadCheckComboBox(string query, string idColumn, string nameColumn)
        {
            secondNameTextBox.Visible = false;
            checkComboBox.Visible = true;
            checkComboBox.Text = "";
            checkComboBox.Items.Clear();

            try
            {
                SqlConnection connection = databaseHelper.GetConnection();
                SqlCommand command = new SqlCommand(query, connection);
                databaseHelper.OpenConnection();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var item = reader[nameColumn].ToString();
                    checkComboBox.Items.Add(item);
                }

                reader.Close();
                databaseHelper.CloseConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка загрузки данных для фильтра: {ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Метод подсчёта нанятых сотрудников
        private int GetHiredCount(string selectedStatus, DateTime startDate, DateTime endDate)
        {
            int hiredCount = 0;
            string procedureName = "GetHiredCount";

            try
            {
                SqlConnection connection = databaseHelper.GetConnection();
                SqlCommand command = new SqlCommand(procedureName, connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@SelectedStatus", selectedStatus);
                command.Parameters.AddWithValue("@StartDate", startDate);
                command.Parameters.AddWithValue("@EndDate", endDate);

                databaseHelper.OpenConnection();
                object result = command.ExecuteScalar();

                if (result != null && result != DBNull.Value)
                {
                    hiredCount = Convert.ToInt32(result);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка при выполнении запроса: {ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                databaseHelper.CloseConnection();
            }

            return hiredCount;
        }

        //Метод подсчёта уволенных сотрудников
        private int GetFiredCount(string selectedStatus, DateTime startDate, DateTime endDate)
        {
            int firedCount = 0;
            string procedureName = "GetFiredCount";

            try
            {
                SqlConnection connection = databaseHelper.GetConnection();
                SqlCommand command = new SqlCommand(procedureName, connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@SelectedStatus", selectedStatus);
                command.Parameters.AddWithValue("@StartDate", startDate);
                command.Parameters.AddWithValue("@EndDate", endDate);

                databaseHelper.OpenConnection();
                object result = command.ExecuteScalar();

                if (result != null && result != DBNull.Value)
                {
                    firedCount = Convert.ToInt32(result);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка при выполнении запроса: {ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                databaseHelper.CloseConnection();
            }

            return firedCount;
        }


        //Обработчик кнопки статистика
        private void ShowStatisticButton_Click(object sender, EventArgs e)
        {
            if (statusComboBox.Text == "")
            {
                MessageBox.Show("Не выбран статус сотрудника", "Предупреждение");
                return;
            }

            string selectedStatus = statusComboBox.SelectedItem.ToString();
            DateTime startDate = startDatePicker.Value.Date;
            DateTime endDate = endDatePicker.Value.Date;
            int count = 0;
            string message = $"За период с {startDate.ToShortDateString()} " +
                $"по {endDate.ToShortDateString()} " +
                $"сотрудников со статусом: {statusComboBox.Text}:\n";

            if (employRBtn.Checked)
            {
                count = GetHiredCount(selectedStatus, startDate, endDate);
                message += $"Нанято: {count}\n";
            }
            else
            {
                count = GetFiredCount(selectedStatus, startDate, endDate);
                message += $"Уволено: {count}";
            }

            MessageBox.Show(message, "Статистика", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }

}
