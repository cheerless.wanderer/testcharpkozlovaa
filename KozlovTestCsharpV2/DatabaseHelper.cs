﻿using Microsoft.Data.SqlClient;
using System.Configuration;

//Класс для работы с БД
public class DatabaseHelper
{
    private SqlConnection connection;
    private string connectionString;

    public DatabaseHelper(string connectionString)
    {
        this.connectionString = connectionString;
        connection = new SqlConnection(connectionString);
    }

    public SqlConnection GetConnection()
    {
        return connection;
    }

    public void OpenConnection()
    {
        if (connection.State == System.Data.ConnectionState.Closed)
        {
            connection.Open();
        }
    }

    public void CloseConnection()
    {
        if (connection.State == System.Data.ConnectionState.Open)
        {
            connection.Close();
        }
    }
}
