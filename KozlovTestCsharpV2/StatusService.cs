﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;

namespace KozlovTestCsharpV2
{
    //Класс для работы с данными о возможных статусах
    public class StatusService
    {
        private DatabaseHelper databaseHelper;

        public StatusService(DatabaseHelper dbHelper)
        {
            databaseHelper = dbHelper;
        }

        //Метод получения списка статусов
        public List<string> GetStatuses()
        {
            List<string> statuses = new List<string>();
            string query = "GetStatuses";

            SqlConnection connection = databaseHelper.GetConnection();
            SqlCommand command = new SqlCommand(query, connection);

            try
            {
                databaseHelper.OpenConnection();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    string statusName = reader.GetString(0);
                    statuses.Add(statusName);
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception($"Ошибка загрузки статусов: {ex.Message}");
            }
            finally
            {
                databaseHelper.CloseConnection();
            }

            return statuses;
        }
    }

}
