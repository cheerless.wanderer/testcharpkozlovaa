﻿namespace KozlovTestCsharpV2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.maintable = new System.Windows.Forms.DataGridView();
            this.filterComboBox = new System.Windows.Forms.ComboBox();
            this.filterBtn = new System.Windows.Forms.Button();
            this.checkComboBox = new System.Windows.Forms.ComboBox();
            this.secondNameTextBox = new System.Windows.Forms.TextBox();
            this.ShowStatisticButton = new System.Windows.Forms.Button();
            this.statusComboBox = new System.Windows.Forms.ComboBox();
            this.statusEmpTextBox = new System.Windows.Forms.TextBox();
            this.endDatePicker = new System.Windows.Forms.DateTimePicker();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.startDatePicker = new System.Windows.Forms.DateTimePicker();
            this.employRBtn = new System.Windows.Forms.RadioButton();
            this.unemployRBtn = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.maintable)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // maintable
            // 
            this.maintable.AllowUserToAddRows = false;
            this.maintable.AllowUserToDeleteRows = false;
            this.maintable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.maintable.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.maintable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.maintable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maintable.Location = new System.Drawing.Point(3, 18);
            this.maintable.Name = "maintable";
            this.maintable.ReadOnly = true;
            this.maintable.RowHeadersWidth = 51;
            this.maintable.RowTemplate.Height = 24;
            this.maintable.Size = new System.Drawing.Size(870, 253);
            this.maintable.TabIndex = 0;
            // 
            // filterComboBox
            // 
            this.filterComboBox.FormattingEnabled = true;
            this.filterComboBox.Items.AddRange(new object[] {
            "По статусу",
            "По отделу",
            "По должности",
            "По части фамилии"});
            this.filterComboBox.Location = new System.Drawing.Point(147, 21);
            this.filterComboBox.Name = "filterComboBox";
            this.filterComboBox.Size = new System.Drawing.Size(121, 24);
            this.filterComboBox.TabIndex = 2;
            this.filterComboBox.SelectedIndexChanged += new System.EventHandler(this.filterComboBox_SelectedIndexChanged);
            // 
            // filterBtn
            // 
            this.filterBtn.Location = new System.Drawing.Point(6, 21);
            this.filterBtn.Name = "filterBtn";
            this.filterBtn.Size = new System.Drawing.Size(135, 28);
            this.filterBtn.TabIndex = 3;
            this.filterBtn.Text = "Отфильтровать";
            this.filterBtn.UseVisualStyleBackColor = true;
            this.filterBtn.Click += new System.EventHandler(this.filterBtn_Click);
            // 
            // checkComboBox
            // 
            this.checkComboBox.FormattingEnabled = true;
            this.checkComboBox.Location = new System.Drawing.Point(273, 19);
            this.checkComboBox.Name = "checkComboBox";
            this.checkComboBox.Size = new System.Drawing.Size(149, 24);
            this.checkComboBox.TabIndex = 5;
            this.checkComboBox.Visible = false;
            // 
            // secondNameTextBox
            // 
            this.secondNameTextBox.Location = new System.Drawing.Point(274, 21);
            this.secondNameTextBox.Name = "secondNameTextBox";
            this.secondNameTextBox.Size = new System.Drawing.Size(148, 22);
            this.secondNameTextBox.TabIndex = 6;
            this.secondNameTextBox.Visible = false;
            // 
            // ShowStatisticButton
            // 
            this.ShowStatisticButton.Location = new System.Drawing.Point(622, 102);
            this.ShowStatisticButton.Name = "ShowStatisticButton";
            this.ShowStatisticButton.Size = new System.Drawing.Size(134, 30);
            this.ShowStatisticButton.TabIndex = 7;
            this.ShowStatisticButton.Text = "Статистика";
            this.ShowStatisticButton.UseVisualStyleBackColor = true;
            this.ShowStatisticButton.Click += new System.EventHandler(this.ShowStatisticButton_Click);
            // 
            // statusComboBox
            // 
            this.statusComboBox.FormattingEnabled = true;
            this.statusComboBox.Location = new System.Drawing.Point(458, 79);
            this.statusComboBox.Name = "statusComboBox";
            this.statusComboBox.Size = new System.Drawing.Size(157, 24);
            this.statusComboBox.TabIndex = 8;
            // 
            // statusEmpTextBox
            // 
            this.statusEmpTextBox.Location = new System.Drawing.Point(458, 54);
            this.statusEmpTextBox.Name = "statusEmpTextBox";
            this.statusEmpTextBox.ReadOnly = true;
            this.statusEmpTextBox.Size = new System.Drawing.Size(157, 22);
            this.statusEmpTextBox.TabIndex = 9;
            this.statusEmpTextBox.Text = "Статус сотрудника";
            this.statusEmpTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // endDatePicker
            // 
            this.endDatePicker.Location = new System.Drawing.Point(252, 79);
            this.endDatePicker.Name = "endDatePicker";
            this.endDatePicker.Size = new System.Drawing.Size(200, 22);
            this.endDatePicker.TabIndex = 10;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(183, 53);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(197, 22);
            this.textBox1.TabIndex = 11;
            this.textBox1.Text = "Временной промежуток";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // startDatePicker
            // 
            this.startDatePicker.Location = new System.Drawing.Point(46, 79);
            this.startDatePicker.Name = "startDatePicker";
            this.startDatePicker.Size = new System.Drawing.Size(200, 22);
            this.startDatePicker.TabIndex = 12;
            // 
            // employRBtn
            // 
            this.employRBtn.AutoSize = true;
            this.employRBtn.Checked = true;
            this.employRBtn.Location = new System.Drawing.Point(622, 51);
            this.employRBtn.Name = "employRBtn";
            this.employRBtn.Size = new System.Drawing.Size(85, 20);
            this.employRBtn.TabIndex = 13;
            this.employRBtn.TabStop = true;
            this.employRBtn.Text = "Нанятые";
            this.employRBtn.UseVisualStyleBackColor = true;
            // 
            // unemployRBtn
            // 
            this.unemployRBtn.AutoSize = true;
            this.unemployRBtn.Location = new System.Drawing.Point(622, 76);
            this.unemployRBtn.Name = "unemployRBtn";
            this.unemployRBtn.Size = new System.Drawing.Size(102, 20);
            this.unemployRBtn.TabIndex = 14;
            this.unemployRBtn.Text = "Уволенные";
            this.unemployRBtn.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.filterBtn);
            this.groupBox1.Controls.Add(this.filterComboBox);
            this.groupBox1.Controls.Add(this.secondNameTextBox);
            this.groupBox1.Controls.Add(this.checkComboBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(470, 57);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.ShowStatisticButton);
            this.groupBox2.Controls.Add(this.unemployRBtn);
            this.groupBox2.Controls.Add(this.statusComboBox);
            this.groupBox2.Controls.Add(this.employRBtn);
            this.groupBox2.Controls.Add(this.statusEmpTextBox);
            this.groupBox2.Controls.Add(this.startDatePicker);
            this.groupBox2.Controls.Add(this.endDatePicker);
            this.groupBox2.Location = new System.Drawing.Point(54, 378);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(762, 138);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.AutoSize = true;
            this.groupBox3.Controls.Add(this.maintable);
            this.groupBox3.Location = new System.Drawing.Point(12, 98);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(876, 274);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 550);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.maintable)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView maintable;
        private System.Windows.Forms.ComboBox filterComboBox;
        private System.Windows.Forms.Button filterBtn;
        private System.Windows.Forms.ComboBox checkComboBox;
        private System.Windows.Forms.TextBox secondNameTextBox;
        private System.Windows.Forms.Button ShowStatisticButton;
        private System.Windows.Forms.ComboBox statusComboBox;
        private System.Windows.Forms.TextBox statusEmpTextBox;
        private System.Windows.Forms.DateTimePicker endDatePicker;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DateTimePicker startDatePicker;
        private System.Windows.Forms.RadioButton employRBtn;
        private System.Windows.Forms.RadioButton unemployRBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}

