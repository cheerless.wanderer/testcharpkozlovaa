﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KozlovTestCsharpV2
{
    //Модель данных, представляющая собой ряд таблицы
    public class Employee
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public DateTime? DateEmploy { get; set; }
        public DateTime? DateUnemploy { get; set; }
        public string StatusName { get; set; }
        public string DepartmentName { get; set; }
        public string PostName { get; set; }
    }

}
