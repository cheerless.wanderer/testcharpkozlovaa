-- �������� ��������� ��� ��������� ������ � ����������� � �������������� �����������
-- ������� ������������� ����������, ���, �������, ��������, ���� ������ �� ������, ���� ���������� (���� ����), �������� �������, ������ � ���������
CREATE PROCEDURE dbo.GetPersonsData
AS
BEGIN
    SELECT 
        p.id, 
        p.second_name + ' ' + LEFT(p.first_name, 1) + '.' + LEFT(p.last_name, 1) + '.' AS full_name, -- ������� � ��������
        p.date_employ, 
        p.date_uneploy, 
        s.name AS status_name, 
        d.name AS department_name, 
        po.name AS post_name 
    FROM 
        dbo.persons p
        LEFT JOIN dbo.deps d ON p.id_dep = d.id
        LEFT JOIN dbo.status s ON p.status = s.id
        LEFT JOIN dbo.posts po ON p.id_post = po.id;
END

GO
-- �������� ��������� ��� ��������� ������ �������� �����������
CREATE PROCEDURE dbo.GetEmployeeStatuses
AS
BEGIN
    SELECT [name] -- �������� �������
    FROM dbo.status;
END;

GO
-- �������� ��������� ��� ��������� ������ �������
CREATE PROCEDURE dbo.GetDepartments
AS
BEGIN
    SELECT name -- �������� ������
    FROM dbo.deps;
END

GO
-- �������� ��������� ��� ��������� ������ ����������
CREATE PROCEDURE dbo.GetPosts
AS
BEGIN
    SELECT name -- �������� ���������
    FROM dbo.posts;
END
GO
-- �������� ��������� ��� ��������� ID ������� �� ��� �����
CREATE PROCEDURE dbo.GetStatusIdByName
    @Name NVARCHAR(100) -- ��� �������
AS
BEGIN
    SELECT id -- ������������� �������
    FROM dbo.status WHERE name = @Name
END
GO
-- �������� ��������� ��� ��������� ID ������ �� ��� �����
CREATE PROCEDURE dbo.GetDepartmentIdByName
    @Name NVARCHAR(100) -- ��� ������
AS
BEGIN
    SELECT id -- ������������� ������
    FROM dbo.deps WHERE name = @Name
END
GO
-- �������� ��������� ��� ��������� ID ��������� �� ��� �����
CREATE PROCEDURE dbo.GetPostIdByName
    @Name NVARCHAR(100) -- ��� ���������
AS
BEGIN
    SELECT id -- ������������� ���������
    FROM dbo.posts WHERE name = @Name
END
GO
-- �������� ��������� ��� ������ ����������� �� ��������� ���������
-- �� �������, ������, ��������� ��� ����� �������
CREATE PROCEDURE dbo.SearchEmployees
    @FilterParam NVARCHAR(100), -- �������� ����������
    @FilterValue NVARCHAR(100) -- �������� �������
AS
BEGIN
    IF @FilterParam = '�� �������'
    BEGIN
        SELECT p.id, p.second_name + ' ' + LEFT(p.first_name, 1) + '.' + LEFT(p.last_name, 1) + '.' AS full_name, -- ������� � ��������
		 p.date_employ, p.date_uneploy, s.name AS status_name, d.name AS department_name, po.name AS post_name
        FROM dbo.persons p
        LEFT JOIN dbo.deps d ON p.id_dep = d.id
        LEFT JOIN dbo.status s ON p.status = s.id
        LEFT JOIN dbo.posts po ON p.id_post = po.id
        WHERE p.status = @FilterValue;
    END
    ELSE IF @FilterParam = '�� ������'
    BEGIN
         SELECT p.id, p.second_name + ' ' + LEFT(p.first_name, 1) + '.' + LEFT(p.last_name, 1) + '.' AS full_name, -- ������� � ��������
		 p.date_employ, p.date_uneploy, s.name AS status_name, d.name AS department_name, po.name AS post_name
        FROM dbo.persons p
        LEFT JOIN dbo.deps d ON p.id_dep = d.id
        LEFT JOIN dbo.status s ON p.status = s.id
        LEFT JOIN dbo.posts po ON p.id_post = po.id
        WHERE p.id_dep = @FilterValue;
    END
    ELSE IF @FilterParam = '�� ���������'
    BEGIN
         SELECT p.id, p.second_name + ' ' + LEFT(p.first_name, 1) + '.' + LEFT(p.last_name, 1) + '.' AS full_name, -- ������� � ��������
		 p.date_employ, p.date_uneploy, s.name AS status_name, d.name AS department_name, po.name AS post_name
        FROM dbo.persons p
        LEFT JOIN dbo.deps d ON p.id_dep = d.id
        LEFT JOIN dbo.status s ON p.status = s.id
        LEFT JOIN dbo.posts po ON p.id_post = po.id
        WHERE p.id_post = @FilterValue;
    END
    ELSE IF @FilterParam = '�� ����� �������'
    BEGIN
         SELECT p.id, p.second_name + ' ' + LEFT(p.first_name, 1) + '.' + LEFT(p.last_name, 1) + '.' AS full_name, -- ������� � ��������
		 p.date_employ, p.date_uneploy, s.name AS status_name, d.name AS department_name, po.name AS post_name
        FROM dbo.persons p
        LEFT JOIN dbo.deps d ON p.id_dep = d.id
        LEFT JOIN dbo.status s ON p.status = s.id
        LEFT JOIN dbo.posts po ON p.id_post = po.id
        WHERE p.second_name LIKE '%' + @FilterValue + '%';
    END
END
GO
-- �������� ��������� ��� ��������� ������ ���� �������� �����������
CREATE PROCEDURE dbo.GetStatuses
AS
BEGIN
    SELECT name -- �������� �������
    FROM dbo.status;
END
GO
-- �������� ��������� ��� ��������� ���������� ������� ����������� � ��������� �������� �� ��������� ������
CREATE PROCEDURE dbo.GetHiredCount
    @SelectedStatus NVARCHAR(100), -- ��������� ������
    @StartDate DATE, -- ��������� ����
    @EndDate DATE -- �������� ����
AS
BEGIN
    DECLARE @StatusId INT;
    SELECT @StatusId = id FROM dbo.status WHERE name = @SelectedStatus;

    SELECT COUNT(*) AS HiredCount
    FROM dbo.persons
    WHERE status = @StatusId
      AND date_employ BETWEEN @StartDate AND @EndDate;
END
GO
-- �������� ��������� ��� ��������� ���������� ��������� ����������� � ��������� �������� �� ��������� ������
CREATE PROCEDURE dbo.GetFiredCount
    @SelectedStatus NVARCHAR(100), -- ��������� ������
    @StartDate DATE, -- ��������� ����
    @EndDate DATE -- �������� ����
AS
BEGIN
    DECLARE @StatusId INT;
    SELECT @StatusId = id FROM dbo.status WHERE name = @SelectedStatus;

    SELECT COUNT(*) AS FiredCount
    FROM dbo.persons
    WHERE status = @StatusId
      AND date_uneploy BETWEEN @StartDate AND @EndDate;
END
